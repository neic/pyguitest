FROM python:3.7

WORKDIR /code

COPY requirements.txt /code/requirements.txt
COPY requirements-test.txt /code/requirements-test.txt

RUN pip install -r requirements.txt -r requirements-test.txt

COPY . /code

CMD ["python", "main.py"]
